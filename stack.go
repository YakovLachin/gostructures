package gostructures

import "fmt"

type ArrayStack struct {
	val []interface{}
}

type StackInterface interface {
	Push(val interface{}) error
	Pop() (interface{}, error)
	Top() (interface{}, error)
}

func NewArrayStack() StackInterface {
	var val []interface{}
	return  &ArrayStack{
		val: val,
	}
}

func (s *ArrayStack) Push(val interface{}) error {
	s.val = append(s.val, val)

	return nil
}

func (s *ArrayStack) Pop() (interface{}, error) {
		if len(s.val) == 0 {
			return nil, fmt.Errorf("Stack Is Empty")
		}
		res := s.val[len(s.val) - 1]
		s.val = s.val[:len(s.val) - 1]
		return res, nil
}

func (s *ArrayStack) Top() (interface{}, error) {
	if len(s.val) == 0 {
		return nil, fmt.Errorf("Stack Is Empty")
	}

	return s.val[len(s.val) - 1], nil
}

type LinkedStack struct {
	head *linkStackSlot
}

type linkStackSlot struct {
	link *linkStackSlot
	val interface{}
}

func NewLinkedStack() StackInterface {
	//noinspection ALL
	return  &LinkedStack{}
}

func (s *LinkedStack) Push(val interface{}) error {
	slot := linkStackSlot{
		val: val,
		link: s.head,
	}
	s.head = &slot

	return nil
}

func (s *LinkedStack) Pop() (interface{}, error) {
	res := s.head.val
	s.head = s.head.link

	return res, nil
}

func (s *LinkedStack) Top() (interface{}, error) {
	return s.head.val, nil
}
