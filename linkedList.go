package gostructures

import "fmt"

type LinkedList struct {
}

func main(){
	node := NewListNode("first")
	secondNode := NewListNode("second")

	node.Add(secondNode)

	fmt.Println(node.GetFirst().(string))
	fmt.Println(node.GetLast().(string))
	fmt.Println(node.getNext())
}


type ListNode struct {
	element interface{}
	next  List
}

type List interface {
	Add(newEntry List) List
	GetFirst() interface{}
	GetLast() interface{}
	getNext() List
}



func NewListNode(element interface{}) List {
	node := ListNode{
		element: element,
	}

	return  &node
}

// в методы к структурам обозначаются
func (e *ListNode) Add(newNode List) List {
	if e.next == nil {
		e.next = newNode
	} else {
		e.next.Add(newNode)
	}


	return e
}

func (e *ListNode) GetFirst() interface{} {
	res := e.element
	//     e.element = e.next.GetFirst()
	//     e.next = e.next.getNext()

	return res
}

func (e *ListNode) GetLast() interface{} {
	if e.next == nil {
		return e.element
	}

	//     e.element = e.next.GetFirst()
	//     e.next = e.next.getNext()

	return e.getNext().GetLast()
}

func (e *ListNode) getNext() List {
	return e.next
}
