package gostructures

type AVLTree interface {
	Insert(AVLTree) AVLTree
	LookUp(int) AVLTree
	ApplyInOrder(TreeFunc, interface{})
	ApplyPostOrder(TreeFunc, interface{})
	GetName() string
	SetName(string)
	GetValue() int
	SetLeft(AVLTree)
	GetLeft() AVLTree
	SetRight(AVLTree)
	GetRight() AVLTree
	getBalance() int
	increaseBalance() int
	decreaseBalance() int
	balance() int
	getParent() AVLTree
	setParent(AVLTree)

}


func InsertIntoAVLTree(parent AVLTree, child AVLTree) AVLTree {
	if parent == nil {
		return child
	}

	child.setParent(parent)
	compareRes := CompareValues(parent.GetValue(), child.GetValue())
	if compareRes > 0 {
		parent.SetRight(InsertIntoAVLTree(parent.GetRight(), child))
		}

	if compareRes < 0 {
		parent.SetLeft(InsertIntoAVLTree(parent.GetLeft(), child))
	}

	return parent
}

func changeBalanceFromLeafToRoot() {

}

func rightRotate(base AVLTree)  {
	left := base.GetLeft()
	leftSubChild := left.GetRight()
	left.SetRight(base)
	base.SetLeft(leftSubChild)
	left.setParent(base.getParent())
	base.setParent(left)
}

func leftRotate (base AVLTree) {
	right := base.GetRight()
	rightSubLeft := right.GetLeft()
	base.SetRight(rightSubLeft)
	rightSubLeft.setParent(base)
	right.SetLeft(base)
	base.setParent(right)
}
