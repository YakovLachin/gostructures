// Package bst provides a set of tools for work with Binary Search Tree elements.
//
// Example Usage
//
// The following is a complete example using nodes in a standard function:
//
//    import (
//    	"gitlab.com/YakovLachin/bst"
//    	"fmt"
//    )
//
//    func main() {
//    	fstNode := bst.NewNode("Root TreeNode", 10)
//    	secNode := bst.NewNode("Right TreeNode", 12)
//    	trdNode := bst.NewNode("Left TreeNode", 9)
//    	forNode := bst.NewNode("Right Right TreeNode", 9)
//
//    	fstNode.Insert(secNode).Insert(trdNode).Insert(forNode)
//    	fstNode.ApplyInOrder(ChangeName, " is Сhanged")
//    	fstNode.ApplyInOrder(printName, "Function does not use second arg")
//    	fstNode.ApplyPostOrder(printName, "Function does not use second arg")
//
//    }
//
//    func printName(tree bst.Tree, _ string)  {
//    	fmt.Printf("%s \n",  tree.GetName())
//    }
//
//    func ChangeName (tree bst.Tree, arg string) {
//    	name := tree.GetName() + arg
//    	tree.SetName(name)
//    }
//
package gostructures
