package gostructures

func InsertToArrHeap(h []int32, val int32) []int32{
	h = append(h, val)
	siftUp(h, len(h)-1)
	return h
}

func siftUp(arr []int32, key int) {
	for arr[key] < arr[(key - 1) / 2] {
		arr[key], arr[(key - 1) / 2] = arr[(key - 1) / 2], arr[key]
		key = (key - 1)/ 2
	}
}
