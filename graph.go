package gostructures

import (
	"math"
)

type Node interface {
	Id() int
	Value() interface{}
}

type GraphNode struct {
	id  int
	val interface{}
}

func (g *GraphNode) Value() interface{} {
	return g.val
}

func (n *GraphNode) Id() int {
	return n.id
}

func NewGraphNode(id int, val interface{}) Node {
	return &GraphNode{
		id:  id,
		val: val,
	}
}

func NewEdge(from, to Node) Edge {
	return &GraphEdge{
		from: from,
		to:   to,
	}
}

type Edge interface {
	From() Node
	To() Node
	Weight() float64
	SetWeight(float64)
}

type GraphInterface interface {
	AddNode(Node)
	AddEdge(Edge)
	GetNode(i int) Node
	MakeNode(i int, val interface{}) Node
	MakeEdge(from, to int)
	TopologicalSort() []Node
	DAGShortestPath(from, to int) ([]float64, []int)
	Dijkstra(from int) ([]float64, []int)
	BellmanFord(from int) ([]float64, []int)
	FindNegativeWeightCycleFrom(from int) []Node
	Remove(i int)
}

type Graph struct {
	Nodes map[int]Node
	Edges map[int]map[int]Edge
}

func (g *Graph) Remove(i int) {
	if _, ok := g.Nodes[i]; ok {
		delete(g.Nodes, i)
	}
}

func (g *Graph) GetEdgesFrom(n Node) map[int]Edge {
	if g.nodeExist(n.Id()) {
		return g.Edges[n.Id()]
	}

	return map[int]Edge{}
}

func (g *Graph) GetNode(i int) Node {
	if g.nodeExist(i) {
		return g.Nodes[i]
	}

	return nil
}

func (g *Graph) nodeExist(i int) bool {
	if g.Nodes[i] == nil {
		return false
	}

	return true
}

func (g *Graph) AddNode(n Node) {
	if !g.nodeExist(n.Id()) {
		g.Nodes[n.Id()] = n
	}
}

func (g *Graph) AddEdge(e Edge) {
	from := e.From().Id()
	to := e.To().Id()

	if g.Edges[from] == nil {
		g.Edges[from] = map[int]Edge{}
	}

	g.Edges[from][to] = e
}

func NewGraph() GraphInterface {
	return &Graph{
		Nodes: map[int]Node{},
		Edges: map[int]map[int]Edge{},
	}
}

func (g *Graph) MakeNode(id int, val interface{}) Node {
	node := NewGraphNode(id, val)
	g.AddNode(node)

	return node
}

func (g *Graph) MakeEdge(from, to int) {
	fromNode := g.GetNode(from)
	toNode := g.GetNode(to)

	e := NewEdge(fromNode, toNode)
	g.AddEdge(e)

	return
}

type GraphEdge struct {
	from   Node
	to     Node
	weight float64
}

func (e *GraphEdge) From() Node {
	return e.from
}

func (e *GraphEdge) To() Node {
	return e.to
}

func (e *GraphEdge) Weight() float64 {
	return e.weight
}

func (e *GraphEdge) SetWeight(i float64) {
	e.weight = i
}

func (g *Graph) TopologicalSort() []Node {
	//создаем список входщей степеней для вершин
	inDegree := map[int]int{}

	//при инициализации входящая степень каждой вершины - нулевая
	for key, _ := range g.Nodes {
		inDegree[key] = 0
	}

	//определяем входящую степень для каждой вершины
	for _, NodeEdges := range g.Edges {
		for key, _ := range NodeEdges {
			inDegree[key]++
		}
	}

	// список вершин с нулевой степенью.
	next := []Node{}
	for key, val := range inDegree {
		if val == 0 {
			next = append(next, g.GetNode(key))
		}
	}

	result := []Node{}

	// пока список вершин с нулевой степенью не пуст.
	for len(next) > 0 {
		// забираем вершину из списка next
		node := next[len(next)-1]
		next = next[:len(next)-1]
		// Добавляем в результат
		result = append(result, node)

		// Уменьшаем входящую степень всех смежных вершин на еденицу
		edges := g.GetEdgesFrom(node)
		for key, _ := range edges {
			inDegree[key]--

			// Если входящая степень смежных вершин ноль добавляем её в список
			// вершин с нулевой входящей степенью.
			if inDegree[key] == 0 {
				next = append(next, g.GetNode(key))
			}
		}
	}

	return result
}

func (g Graph) DAGShortestPath(from, to int) ([]float64, []int) {
	nodes := g.TopologicalSort()
	lenght := len(nodes)
	// Кратчайшие пути до вершин.
	shortest := make([]float64, lenght, lenght)

	// предшественники на пути к требуемой вершине
	pred := make([]int, lenght, lenght)

	// Так как ни один путь еще не известен,
	// Расстояние до любой точки равно +бесконечность
	// А индекс предшесвенников ставим не известным(-1)
	for key := range nodes {
		shortest[key] = math.Inf(1)
		pred[key] = -1
	}
	// Расстояние от себя к самому себе равно 0
	shortest[from] = 0

	//
	for _, node := range nodes {
		for toEdgeKey := range g.Edges[node.Id()] {
			shortest, pred = g.relax(node.Id(), toEdgeKey, shortest, pred)
		}
	}

	return shortest, pred
}

func (g *Graph) Dijkstra(from int) ([]float64, []int) {
	nodes := g.Nodes
	lenght := len(nodes)

	// Кратчайшие пути до вершин.
	shortest := make([]float64, lenght, lenght)

	// предшественники на пути к требуемой вершине
	pred := make([]int, lenght, lenght)

	// Так как ни один путь еще не известен,
	// Расстояние до любой точки равно +бесконечность
	// А индекс предшесвенников ставим не известным(-1)
	for key := range nodes {
		shortest[key] = math.Inf(1)
		pred[key] = -1
	}
	// Расстояние от себя к самому себе равно 0
	shortest[from] = 0

	for len(nodes) > 0 {
		smallestShortest := math.Inf(1)
		var smallestIndex int
		for i, v := range shortest {
			if _, ok := nodes[i]; !ok {
				continue
			}

			if smallestShortest > v {
				smallestShortest = v
				smallestIndex = i
			}
		}

		delete(nodes, smallestIndex)

		for toEdgeKey := range g.Edges[smallestIndex] {
			shortest, pred = g.relax(smallestIndex, toEdgeKey, shortest, pred)
		}
	}

	return shortest, pred
}

func (g *Graph) relax(from, to int, shortest []float64, pred []int) ([]float64, []int) {
	current := shortest[from] + g.weightEdges(from, to)
	if current < shortest[to] {
		shortest[to] = current
		pred[to] = from
	}

	return shortest, pred
}

// Возвращает вес ребра.
func (g *Graph) weightEdges(from, to int) float64 {
	if edges, ok := g.Edges[from]; ok {
		if edge, ok := edges[to]; ok {
			return edge.Weight()
		}
	}

	return math.Inf(1)
}

func (g *Graph) BellmanFord(from int) ([]float64, []int) {
	nodes := g.Nodes
	lenght := len(nodes)

	// Кратчайшие пути до вершин.
	shortest := make([]float64, lenght, lenght)

	// предшественники на пути к требуемой вершине
	pred := make([]int, lenght, lenght)

	// Так как ни один путь еще не известен,
	// Расстояние до любой точки равно +бесконечность
	// А индекс предшесвенников ставим не известным(-1)
	for key := range nodes {
		shortest[key] = math.Inf(1)
		pred[key] = -1
	}
	shortest[from] = 0

	edges := g.Edges
	for i := 1; i < lenght-1; i++ {
		for kFrom := range edges {
			for kTo := range edges[kFrom] {
				shortest, pred = g.relax(kFrom, kTo, shortest, pred)
			}
		}
	}

	return shortest, pred
}

func (g *Graph) FindNegativeWeightCycleFrom(from int) []Node {
	shortest, pred := g.BellmanFord(from)

	edges := g.Edges

	negativeEdges := []Edge{}

	for kFrom := range edges {
		for kTo := range edges[kFrom] {
			current := shortest[kFrom] + g.weightEdges(kFrom, kTo)
			if current < shortest[kTo] {
				negativeEdges = append(negativeEdges, edges[kFrom][kTo])
			}
		}
	}

	if len(negativeEdges) == 0 {
		return []Node{}
	}

	visited := map[int]bool{}
	for key := range g.Edges {
		visited[key] = false
	}

	negIndex := negativeEdges[0].To().Id()
	negNodeX := g.Nodes[negIndex]

	visited[negIndex] = true
	negIndex = pred[negIndex]

	predNodeV := g.Nodes[negIndex]

	cycle := []Node{}
	cycle = append(cycle, negNodeX)

	for predNodeV != negNodeX {
		cycle = append([]Node{predNodeV}, cycle...)
		id := predNodeV.Id()
		predNodeV = g.Nodes[pred[id]]
	}

	return cycle
}
