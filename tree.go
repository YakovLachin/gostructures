package gostructures

type Tree interface {
	Insert(Tree) Tree
	LookUp(int) Tree
	ApplyInOrder(TreeFunc, interface{})
	ApplyPostOrder(TreeFunc, interface{})
	GetName() string
	SetName(string)
	GetValue() int
	SetLeft(Tree)
	GetLeft() Tree
	SetRight(Tree)
	GetRight() Tree
}

type TreeFunc func(Tree, interface{})

type TreeNode struct {
	name  string
	val   int
	left  Tree
	right Tree
	balance int
}

func NewNode(name string, val int) Tree {
	node := TreeNode{
		name: name,
		val:  val,
	}

	return &node
}

func (n *TreeNode) GetValue() int {
	return n.val
}

func (n *TreeNode) GetName() string {
	return n.name
}

func (n *TreeNode) SetName(name string) {
	n.name = name
}

func (n *TreeNode) SetLeft(l Tree) {
	n.left = l
}

func (n *TreeNode) SetRight(r Tree) {
	n.right = r
}

func (n *TreeNode) GetRight() Tree {
	return n.right
}

func (n *TreeNode) GetLeft() Tree {
	return n.left
}

func InsertIntoTree(treep Tree, newp Tree) Tree {
	if treep == nil {
		return newp
	}

	cmpRes := CompareValues(newp.GetValue(), treep.GetValue())
	if cmpRes < 0 {
		treep.SetLeft(InsertIntoTree(treep.GetLeft(), newp))
	}

	if cmpRes > 0 {
		treep.SetRight(InsertIntoTree(treep.GetRight(), newp))
	}

	return treep
}

// Insert new TreeNode in Tree
func (treep *TreeNode) Insert(newp Tree) Tree {

	cmpRes := CompareValues(newp.GetValue(), treep.GetValue())
	if cmpRes < 0 {
		if treep.GetLeft() == nil {
			treep.SetLeft(newp)
		} else {
			treep.SetLeft(treep.GetLeft().Insert(newp))
		}
	}

	if cmpRes > 0 {
		if treep.GetRight() == nil {
			treep.SetRight(newp)
		} else {
			treep.SetRight(treep.GetRight().Insert(newp))
		}
	}

	return treep
}

// Return TreeNode By required val
func (treep *TreeNode) LookUp(val int) Tree {
	var cmp int

	cmp = CompareValues(treep.GetValue(), val)

	if cmp == 0 {
		return treep
	}

	if cmp > 0 && treep.GetLeft() != nil {
		return treep.GetLeft().LookUp(val)
	}

	if cmp < 0 && treep.GetRight() != nil {
		return treep.GetRight().LookUp(val)
	}

	return nil
}

//
func (treep *TreeNode) ApplyInOrder(void TreeFunc, arg interface{}) {
	if treep.GetLeft() != nil {
		treep.GetLeft().ApplyInOrder(void, arg)
	}
	void(treep, arg)

	if treep.GetRight() != nil {
		treep.GetRight().ApplyInOrder(void, arg)
	}
}

func (treep *TreeNode) ApplyPostOrder(void TreeFunc, arg interface{}) {
	if treep.GetLeft() != nil {
		treep.GetLeft().ApplyInOrder(void, arg)
	}

	if treep.GetRight() != nil {
		treep.GetRight().ApplyInOrder(void, arg)
	}

	void(treep, arg)
}

func CompareValues(fst int, sec int) int {
	if fst > sec {
		return 1
	}

	if fst < sec {
		return -1
	}

	return 0
}
