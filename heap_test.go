package gostructures

import (
	"fmt"
	"testing"
)

func Test_InsertInArrHeap_ShouldBuildHeap(t *testing.T) {
	heap := make([]int32, 0, 9)
	heap = InsertToArrHeap(heap, 12)
	heap = InsertToArrHeap(heap, 22)
	heap = InsertToArrHeap(heap, 18)
	heap = InsertToArrHeap(heap, 1)
	heap = InsertToArrHeap(heap, 25)
	heap = InsertToArrHeap(heap, 14)
	heap = InsertToArrHeap(heap, 16)
	heap = InsertToArrHeap(heap, 23)
	heap = InsertToArrHeap(heap, 10)
	heap = InsertToArrHeap(heap, 11)
	heap = InsertToArrHeap(heap, 9)
	heap = InsertToArrHeap(heap, 6)
	fmt.Printf("%v \n", heap)
}
