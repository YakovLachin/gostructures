package gostructures

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGraphNewGraph_AddNodesAndEdge_ReturnGraph(t *testing.T) {
	node := NewGraphNode(1, "graph1")
	node2 := NewGraphNode(2, "graph2")
	graph := NewGraph()
	graph.AddNode(node)
	graph.AddNode(node2)
	edge := NewEdge(node, node2)
	graph.AddEdge(edge)

	actual := graph.GetNode(1).Value()
	assert.Equal(t, "graph1", actual.(string))
}

func TestGraphNewGraph_MakeNodesAndEdge_ReturnGraph(t *testing.T) {
	graph := NewGraph()
	graph.MakeNode(1, "graph1")
	graph.MakeNode(2, "graph2")
	graph.MakeEdge(1, 2)

	actual := graph.GetNode(1).Value()
	assert.Equal(t, "graph1", actual.(string))
}

func TestTopologicalSort(t *testing.T) {
	g := NewGraph()
	fNode := NewGraphNode(1, "firstValue")
	g.AddNode(fNode)

	sNode := NewGraphNode(2, "secondValue")
	g.AddNode(sNode)

	tNode := NewGraphNode(3, "thrdValue")
	g.AddNode(tNode)

	foNode := NewGraphNode(4, "fthValue")
	g.AddNode(foNode)

	fvNode := NewGraphNode(5, "fiveValue")
	g.AddNode(fvNode)

	g.MakeEdge(4, 2)
	g.MakeEdge(4, 3)
	g.MakeEdge(2, 5)
	g.MakeEdge(3, 5)
	g.MakeEdge(1, 4)

	actual := g.TopologicalSort()
	expected := []Node{
		fNode,
		foNode,
		tNode,
		sNode,
		fvNode,
	}

	assert.Equal(t, expected, actual)
}

func TestGraph_DAGShortestPath(t *testing.T) {
	g := initGraph()
	shortests, pred := g.DAGShortestPath(1, 5)

	inf := math.Inf(1)
	expectedShortest := []float64{inf, 0, 9, 7, 3, 6}
	ExpectedPred := []int{-1, -1, 4, 4, 1, 2}

	assert.Equal(t, expectedShortest, shortests)
	assert.Equal(t, ExpectedPred, pred)
}

func TestGraph_Dijkstra(t *testing.T) {
	g := initGraph()
	shortests, pred := g.Dijkstra(1)

	inf := math.Inf(1)
	expectedShortest := []float64{inf, 0, 9, 7, 3, 6}
	ExpectedPred := []int{-1, -1, 4, 4, 1, 2}

	assert.Equal(t, expectedShortest, shortests)
	assert.Equal(t, ExpectedPred, pred)
}

func TestGraph_BelmanFord(t *testing.T) {
	g := initGraph()
	shortests, pred := g.BellmanFord(1)
	inf := math.Inf(1)
	expectedShortest := []float64{inf, 0, 9, 7, 3, 6}
	ExpectedPred := []int{-1, -1, 4, 4, 1, 2}

	assert.Equal(t, expectedShortest, shortests)
	assert.Equal(t, ExpectedPred, pred)
}

func TestGraph_FindNegativeWeightCycle_CycleWithTwoNodes_ReturnTwoNodes(t *testing.T) {
	g := initGraphWithNegativeCycle()
	nodes := g.FindNegativeWeightCycleFrom(1)
	expected := []Node{
		g.GetNode(2),
		g.GetNode(5),
	}

	assert.Equal(t, expected, nodes)
}

func TestGraph_FindNegativeWeightCycle_CycleWithFoureNodes_ReturnFoureNodes(t *testing.T) {
	g := initGraphWithNegativeCycle2()
	nodes := g.FindNegativeWeightCycleFrom(1)
	expected := []Node{
		g.GetNode(6),
		g.GetNode(7),
		g.GetNode(2),
		g.GetNode(5),
	}

	assert.Equal(t, expected, nodes)
}

//
// ниже нарисована структура графа.
//
// ⭢ - положительное ребро
// ⭲ - отрицательное ребро
//
//             3
//           🡕  🡖
// 0 🡒 1 🡒 4     5
//           🡖  ⭷
//             2
func initGraph() GraphInterface {
	g := NewGraph()
	zero := NewGraphNode(0, "zeroValue")
	g.AddNode(zero)

	first := NewGraphNode(1, "firstValue")
	g.AddNode(first)

	second := NewGraphNode(2, "secondValue")
	g.AddNode(second)

	three := NewGraphNode(3, "thrdValue")
	g.AddNode(three)

	foure := NewGraphNode(4, "fthValue")
	g.AddNode(foure)

	five := NewGraphNode(5, "fiveValue")
	g.AddNode(five)

	zeroToFirst := NewEdge(zero, first)
	zeroToFirst.SetWeight(2)
	g.AddEdge(zeroToFirst)

	firstToFoure := NewEdge(first, foure)
	firstToFoure.SetWeight(3)
	g.AddEdge(firstToFoure)

	foureToTwo := NewEdge(foure, second)
	foureToTwo.SetWeight(6)
	g.AddEdge(foureToTwo)

	foureToThree := NewEdge(foure, three)
	foureToThree.SetWeight(4)
	g.AddEdge(foureToThree)

	threeToFive := NewEdge(three, five)
	threeToFive.SetWeight(2)
	g.AddEdge(threeToFive)

	twoToFive := NewEdge(second, five)
	twoToFive.SetWeight(-3)
	g.AddEdge(twoToFive)

	return g
}

//
// ниже нарисована структура графа.
//
// ⭢ - положительное ребро
// ⭲ - отрицательное ребро
//
//             3
//           🡕  🡖
// 0 🡒 1 🡒 4     5
//           🡖  ⭷⭹
//             2
//
// цикл 2 5
//
func initGraphWithNegativeCycle() GraphInterface {
	g := NewGraph()
	zero := NewGraphNode(0, "zeroValue")
	g.AddNode(zero)

	first := NewGraphNode(1, "firstValue")
	g.AddNode(first)

	second := NewGraphNode(2, "secondValue")
	g.AddNode(second)

	three := NewGraphNode(3, "thrdValue")
	g.AddNode(three)

	foure := NewGraphNode(4, "fthValue")
	g.AddNode(foure)

	five := NewGraphNode(5, "fiveValue")
	g.AddNode(five)

	zeroToFirst := NewEdge(zero, first)
	zeroToFirst.SetWeight(2)
	g.AddEdge(zeroToFirst)

	firstToFoure := NewEdge(first, foure)
	firstToFoure.SetWeight(3)
	g.AddEdge(firstToFoure)

	foureToTwo := NewEdge(foure, second)
	foureToTwo.SetWeight(6)
	g.AddEdge(foureToTwo)

	foureToThree := NewEdge(foure, three)
	foureToThree.SetWeight(4)
	g.AddEdge(foureToThree)

	threeToFive := NewEdge(three, five)
	threeToFive.SetWeight(2)
	g.AddEdge(threeToFive)

	twoToFive := NewEdge(second, five)
	twoToFive.SetWeight(2)
	g.AddEdge(twoToFive)

	fiveToTwo := NewEdge(five, second)
	fiveToTwo.SetWeight(-2)
	g.AddEdge(fiveToTwo)

	return g
}



//
// ниже нарисована структура графа.
//
// ⭢ - положительное ребро
// ⭲ - отрицательное ребро
//
//             3
//           🡕  🡖
// 0 🡒 1 🡒 4     5 🡒 6
//           🡖  ⭷  ⭨ ⭹
//             2  ⭰ 7
//
// цикл 6 7 2 5
//
func initGraphWithNegativeCycle2() GraphInterface {
	g := NewGraph()
	zero := NewGraphNode(0, "zeroValue")
	g.AddNode(zero)

	first := NewGraphNode(1, "firstValue")
	g.AddNode(first)

	second := NewGraphNode(2, "secondValue")
	g.AddNode(second)

	three := NewGraphNode(3, "thrdValue")
	g.AddNode(three)

	foure := NewGraphNode(4, "fthValue")
	g.AddNode(foure)

	five := NewGraphNode(5, "fiveValue")
	g.AddNode(five)

	six := NewGraphNode(6, "sixValue")
	g.AddNode(six)

	seven := NewGraphNode(7, "sevenValue")
	g.AddNode(seven)

	zeroToFirst := NewEdge(zero, first)
	zeroToFirst.SetWeight(2)
	g.AddEdge(zeroToFirst)

	firstToFoure := NewEdge(first, foure)
	firstToFoure.SetWeight(3)
	g.AddEdge(firstToFoure)

	foureToTwo := NewEdge(foure, second)
	foureToTwo.SetWeight(6)
	g.AddEdge(foureToTwo)

	foureToThree := NewEdge(foure, three)
	foureToThree.SetWeight(4)
	g.AddEdge(foureToThree)

	threeToFive := NewEdge(three, five)
	threeToFive.SetWeight(2)
	g.AddEdge(threeToFive)

	twoToFive := NewEdge(second, five)
	twoToFive.SetWeight(2)
	g.AddEdge(twoToFive)

	fiveToSix := NewEdge(five, six)
	fiveToSix.SetWeight(3)
	g.AddEdge(fiveToSix)

	fiveToSeven := NewEdge(five, six)
	fiveToSeven.SetWeight(2)
	g.AddEdge(fiveToSix)

	sixToSeven := NewEdge(six, seven)
	sixToSeven.SetWeight(-3)
	g.AddEdge(sixToSeven)

	sevenToTwo := NewEdge(seven, second)
	sevenToTwo.SetWeight(-3)
	g.AddEdge(sevenToTwo)

	return g
}
