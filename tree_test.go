package gostructures

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewNode_ReturnNode(t *testing.T) {
	node := NewNode("Name", 10)
	assert.Equal(t, "Name", node.GetName())
	assert.Equal(t, 10, node.GetValue())
}

func TestCompare_fstMoreThanSec_Return1(t *testing.T) {
	actual := CompareValues(1, 0)
	assert.Equal(t, 1, actual)
}

func TestCompare_fstLessThanSec_ReturnNegative1(t *testing.T) {
	actual := CompareValues(1, 2)
	assert.Equal(t, -1, actual)
}

func TestCompare_Equal_ReturnZero(t *testing.T) {

	actual := CompareValues(1, 1)
	assert.Equal(t, 0, actual)
}

func TestInsert_InsertingNodeLessThanRootNode_NodeInsertingInLeft(t *testing.T) {
	node := NewNode("Root TreeNode", 100)
	insNode := NewNode("Inserting TreeNode", 1)

	node.Insert(insNode)
	assert.Equal(t, insNode, node.GetLeft())
	assert.Equal(t, nil, node.GetRight())
}

func TestInsert_InsertingNodeMoreThanRootNode_NodeInsertingInRight(t *testing.T) {
	node := NewNode("Root TreeNode", 100)
	insNode := NewNode("Inserting TreeNode", 101)

	node.Insert(insNode)
	assert.Equal(t, insNode, node.GetRight())
	assert.Equal(t, nil, node.GetLeft())
}

func TestInsert_InsertingNodeEqualToRootNode_NodeIsNotInserting(t *testing.T) {
	node := NewNode("Root TreeNode", 100)
	insNode := NewNode("Inserting TreeNode", 100)

	node.Insert(insNode)
	assert.Equal(t, nil, node.GetRight())
	assert.Equal(t, nil, node.GetLeft())
}

func TestLookUp_GetValueFromRightNode_ReturnRightNode(t *testing.T) {
	node := NewNode("Root TreeNode", 10)
	rightNode := NewNode("Right TreeNode", 100)

	node.SetRight(rightNode)

	resultNode := node.LookUp(100)
	assert.Equal(t, rightNode, resultNode)
}

func TestLookUp_GetValueFromLeftNode_ReturnRightNode(t *testing.T) {
	node := NewNode("Root TreeNode", 10)
	rightNode := NewNode("Right TreeNode", 100)
	leftNode := NewNode("Left TreeNode", 1)

	node.SetRight(rightNode)
	node.SetLeft(leftNode)

	resultNode := node.LookUp(1)
	assert.Equal(t, leftNode, resultNode)
}

func TestLookUp_GetValueFromRoot_ReturnRoot(t *testing.T) {
	node := NewNode("Root TreeNode", 10)

	resultNode := node.LookUp(10)
	assert.Equal(t, node, resultNode)
}

func TestLookUp_GetDoesNotExistingValue_ReturnNil(t *testing.T) {
	node := NewNode("Root TreeNode", 10)

	assert.Equal(t, nil, node.LookUp(1))
}

func ChangeName(tree Tree, arg interface{}) {
	tree.SetName(arg.(string))
}

func TestApplyInOrder_ApplyValues_NodeIsApplied(t *testing.T) {
	rootNode := NewNode("Root TreeNode", 2)
	secNode := NewNode("Second TreeNode", 1)
	thrNode := NewNode("Third TreeNode", 3)

	rootNode.Insert(secNode)
	rootNode.Insert(thrNode)

	rootNode.ApplyInOrder(ChangeName, "ChangedName")

	assert.Equal(t, "ChangedName", rootNode.GetName())
	assert.Equal(t, "ChangedName", secNode.GetName())
	assert.Equal(t, "ChangedName", thrNode.GetName())
}

func TestApplyPostOrder_ApplyValues_NodeIsApplied(t *testing.T) {
	rootNode := NewNode("Root TreeNode", 2)
	secNode := NewNode("Second TreeNode", 1)
	thrNode := NewNode("Third TreeNode", 3)

	rootNode.Insert(secNode)
	rootNode.Insert(thrNode)

	rootNode.ApplyPostOrder(ChangeName, "ChangedName")

	assert.Equal(t, "ChangedName", rootNode.GetName())
	assert.Equal(t, "ChangedName", secNode.GetName())
	assert.Equal(t, "ChangedName", thrNode.GetName())
}
