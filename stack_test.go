package gostructures

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

type val int

func TestArrayStack_PushValue_ValueIsPushed(t *testing.T) {
	stack := NewArrayStack()
	v := 1
	err := stack.Push(v)
	assert.Nil(t, err)
	res, err := stack.Pop()
	assert.Nil(t, err)
	assert.Equal(t, 1, res)
}

func TestArrayStack_PushValueTwice_PopLastValue(t *testing.T) {
	stack := NewArrayStack()
	v := val(1)
	v2 := val(2)
	err := stack.Push(v)
	assert.Nil(t, err)
	err = stack.Push(v2)
	assert.Nil(t, err)
	res, err := stack.Pop()
	assert.Nil(t, err)
	assert.Equal(t, val(2), res)
}

func TestArrayStack_PushAndPopValueTwice_PopFirstValue(t *testing.T) {
	stack := NewArrayStack()
	v := val(1)
	v2 := val(2)
	stack.Push(v)
	stack.Push(&v2)
	topVal, _ := stack.Top()
	vall := topVal.(*val)
	assert.Equal(t, val(2), *vall)
	*vall = val(3)
	topVal, _ = stack.Top()
	newVall := topVal.(*val)
	assert.Equal(t, val(3), *newVall)
}

func TestLinkedStack_Push(t *testing.T) {
	stack := NewLinkedStack()
	stack.Push(2)
	stack.Push(3)
	actual,  err := stack.Pop()
	assert.Equal(t, 3, actual)
	actual,  err = stack.Pop()
	assert.Equal(t, 2, actual)
	assert.Nil(t, err)
}
