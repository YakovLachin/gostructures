package gostructures

// `limit` times increase value `n` at every iteration
func GeneratorInt(n int, limit int) chan int {
	ch := make(chan int)
	go func() {
		defer close(ch)
		for i:= 0; i < limit; i++ {
			ch <- n + i
		}
	}()

	return ch
}
